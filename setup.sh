#!/bin/bash

function cleanup {
  if [ -z $SKIP_CLEANUP ]; then
    echo "Removing build directory ${BUILDENV}"
    rm -rf "${BUILDENV}"
  fi
}

#trap cleanup EXIT

BUILDENV=`mktemp -d /tmp/WPtest.XXXXXXXX`
ssh-agent /bin/bash -c "ssh-add $HOME/.ssh/deployment; git clone git@bitbucket.org:synergoteam/wp_teststand.git $BUILDENV/wp_teststand"

if [ -d ~/Synergo ]; then
mkdir -p ~/Synergo
fi

if [ -d ~/Synergo/Wordpress ]; then
mkdir -p ~/Synergo/Wordpress
fi

echo Downloading Wordpress version $WP_VERSION

echo Checking From Cache . . .

if [ -d ~/Synergo/Wordpress/$WP_VERSION ]; then
   echo Version $WP_VERSION found checking for latest commit
   cd ~/Synergo/Wordpress/$WP_VERSION
   git pull
   if [ ! -d $BUILDENV/htdocs ]; then
     mkdir $BUILDENV/htdocs
   fi
   cp -ar . $BUILDENV/htdocs

else
   echo Version $WP_VERSION not found. Clonning from source . .
   git clone --branch $WP_VERSION https://github.com/wordpress/wordpress --depth 1 ~/Synergo/Wordpress/$WP_VERSION
   if [ ! -d $BUILDENV/htdocs ]; then
      mkdir $BUILDENV/htdocs
   fi
   cp -ar ~/Synergo/Wordpress/$WP_VERSION/. $BUILDENV/htdocs
fi

cd $WORKSPACE

theme=$( grep -r 'Theme Name' )
style=$( ls style.css )

if [ -n "$theme" -a -n "$style" ] ; then
export proj_dir="${BUILDENV}/htdocs/wp-content/themes/project"
mkdir -p $proj_dir
cp -r "${WORKSPACE}/." "${proj_dir}"
else
export proj_dir="${BUILDENV}/htdocs/wp-content/themes/project"
mkdir -p $proj_dir
cp -r "${WORKSPACE}/." "${proj_dir}"
fi

cd $BUILDENV

cd /${BUILDENV}/wp_teststand

if [ -f /var/lib/jenkins/workspace/vendor/bin/phpunit -a -f ${BUILDENV}/wp_teststand/phpunit.xml -a -f ${BUILDENV}/wp_teststand/bootstrap.php -a -d ${BUILDENV}/wp_teststand/includes ]; then
/var/lib/jenkins/workspace/vendor/bin/phpunit --coverage-html $WORKSPACE/build/coverage --coverage-clover $WORKSPACE/build/logs/clover.xml --coverage-crap4j $WORKSPACE/build/logs/crap4j.xml  --log-junit $WORKSPACE/build/logs/junit.xml --test-suffix .php $proj_dir/tests --verbose
else
exit
fi
